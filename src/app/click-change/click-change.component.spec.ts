import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClickChangeComponent } from './click-change.component';

describe('ClickChangeComponent', () => {
  let component: ClickChangeComponent;
  let fixture: ComponentFixture<ClickChangeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClickChangeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ClickChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
