import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Book } from "./book";

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http:HttpClient) { }

  search(query: string) {
    return this.http.get<SearchResult>('http://openlibrary.org/search.json?q=' + query);
  }
}

export interface SearchResult {
  numFound:number;
  docs: Book[];
}
