import {Component, Input, OnInit} from '@angular/core';
import {BookService, SearchResult} from "../book.service";
import {Book} from "../book";

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  @Input()
  book?: Book;

  constructor(private bookService:BookService) { }

  ngOnInit(): void {
  }

}
