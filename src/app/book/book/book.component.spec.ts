import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Book } from "../book";


import { BookComponent } from './book.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('BookComponent', () => {
  let component: BookComponent;
  let fixture: ComponentFixture<BookComponent>;
  let testBook: Book;
  let compiled: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      declarations: [BookComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    testBook = {
      title: 'title of the book',
      first_publish_year: 999,
      author_name: ['author of the book', 'co-author of it too'],
      cover_i: '1',
      subject: ['subject', 'other subject'],
    }

    fixture = TestBed.createComponent(BookComponent);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render book info', function () {
    component.book = testBook;
    fixture.detectChanges();
    expect(compiled.querySelector('h3')?.textContent).toBe('title of the book');
    expect(compiled.innerHTML).toContain('Author: author of the book');
    expect(compiled.innerHTML).toContain('Genres: subject');
    expect(compiled.innerHTML).toContain('Published: 999');
  });


  it('should display unknown author when no author', () => {
    component.book = testBook;
    component.book.author_name = undefined;
    fixture.detectChanges();
    expect(compiled.innerHTML).toContain('Author: Unknown');
  });
});

