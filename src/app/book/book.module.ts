import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchPageComponent } from './search-page/search-page.component';
import { BookRoutingModule } from './book-routing.module';
import { BookComponent } from './book/book.component';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    SearchPageComponent,
    BookComponent
  ],
  imports: [
    CommonModule,
    BookRoutingModule,
    HttpClientModule,
    FormsModule,
  ]
})
export class BookModule { }
