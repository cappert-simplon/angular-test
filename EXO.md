# Modules et Testing

https://gitlab.com/simplonlyon/promo18/angular-test

Créer un projet angular-test (ou angular-book), avec routing

https://openlibrary.org/dev/docs/api/search
> Internet Archive Open Library is an open, editable library catalog, building towards a web page for every book ever published. Read, borrow, and discover more than 3M books for free.

1. Générer un BookModule avec ng g m book, et dans ce module, générer un BookService (pour générer dans un module/dossier, on peut rajouter par exemple book/ devant le nom du truc à générer)
2. En regardant les exemples de l'api openlibrary, créer une interface Book dans laquelle j'aimerais qu'on puisse récupérer au moins : le titre, l'année de publication, le ou les auteur·ices, le genre et pourquoi pas la couverture (faire ça dans un fichier entities dans le dossier app)
3. Dans le BookService, créer une interface SearchResult qui contiendra le numFound en number et les docs en tableau de Book
4. Dans le BookModule, rajouter le HttpClient dans les imports, et dans le BookService, faire une méthode search qui va attendre une string en argument et renvoyer un SearchResult
5. Créer un component SearchPage dans le dossier book qui va pour le moment juste afficher un formulaire de recherche (un input et un button)
6. Dans le dossier book, créer un fichier book-routing.module.ts et mettre ça dedans :
```
   const routes: Routes = [

];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class BookRoutingModule { }
```

7. Dans les routes de ce fichier, rajouter une route search qui pointera sur le SearchPageComponent, puis ajouter le BookRoutingModule dans les listes des imports BookModule et le BookModule dans les imports du AppModule (modifié)


Pour la cover, on peut prendre cette url : https://covers.openlibrary.org/b/id/10523169-M.jpg et remplacer le numéro par la propriété cover_i du document (si elle existe)

## La recherche dans le SearchPageComponent :
1. Créer un BookComponent qui va attendre en @Input un Book, et faire l'affichage de ce book comme vous le souhaitez
2. Dans le BookModule, rajouter le FormsModule dans les imports
3. Dans le SearchPageComponent, rajouter une propriété query et la lier avec un ngModel à l'input, puis faire qu'au ngSubmit on déclenche la méthode search du service
4. Stocker le retour du search dans une propriété du SearchPageComponent et dans le template faire un boucle ngFor sur la liste de docs, et à chaque tour de boucle, afficher un app-book

Bonus: Faire en sorte que ça affiche les covers s'il y en a, et si yen a pas, que ça affiche une image par défaut à la place

## Tests

### Premier test :
Tester le ClickChangeComponent en vous inspirant de ce qu'il y a dans le app.component.spec.ts

A tester :
* L'affichage initial du component (donc il est sensé avoir 'Coucou' à l'intérieur d'un paragraphe)
* Le fait que l'affichage ait changé en 'autre chose' une fois que la méthode 'action' a été déclenchée

### Tester le BookComponent

* Commencer par faire un test qui va assigner à la propriété book du component un objet Book de test avec des valeurs à la con puis vérifier avec le compiled et des querySelector ou autre que sont bien affichés dans le templates les différentes informations du book

Choses à tester en plus qui demanderont de modifier le template du `BookComponent`:
* Faire un test qui va vérifier que si la propriété author du book est vide, alors ça affiche author: Unknown dans le template (et modifier le component en conséquence)
* Faire un test qui va vérifier que s'il n'y a pas de subject, alors on affichera pas la ligne avec Genres:  dans le template
* Faire un test qui va vérifier que s'il n'y a pas de cover_i alors il n'y aura pas d'élément img de render

### Tester le SearchPageComponent en utilisant un spy

1. Créer un spyobj pour le `BookService` comme on a fait dans l'example avec le randomizer
2. Créer un scénario de test dans lequel on va dire à notre spy que lorsqu'on appel sa méthode `search`, il est sensé renvoyé un `Observable` de `SearchResult` (pour ça, vous pouvez utiliser le `of({numFounds: 2, ...})` comme valeur de retour)
3. Faire une vérification sur le template qu'on a bien le h2 qui s'affiche par exemple et aussi pourquoi pas qu'on a le nombre d'élément de type app-book correspondant à ce que vous avez dit que votre spy renvoie

Bonus : Faire le test avec le HttpTestingController plutôt qu'avec un spy
